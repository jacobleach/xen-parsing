module FileUtils
  (
    loadFile
  , getAllThreadFileNames
  )
  where

import Prelude hiding (readFile)
import System.FilePath.Glob
import Data.ByteString (ByteString, readFile)
import Text.HTML.TagSoup.Fast (parseTags)
import Text.HTML.TagSoup (Tag)

import Debug.Trace (trace)

loadFile :: String -> IO (Maybe [Tag ByteString])
loadFile fileName = do
  putStrLn $ "Loading file: " ++ fileName

  file <- readFile fileName
  let parsedFile = case parseTags file of
                    [] -> trace ("Failed to parse file: " ++ fileName) Nothing
                    xs -> Just xs

  return parsedFile


getAllThreadFileNames :: String -> IO ([String])
getAllThreadFileNames directory = globDir1 (compile ("thread-*-page*.txt")) directory

