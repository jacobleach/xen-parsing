{-# LANGUAGE RecordWildCards #-}

module Types
  (
    Forum (..)
  , Post (..)
  , Thread (..)
  , User (..)
  , toRow
  , toField
  ) where

import Database.PostgreSQL.Simple.ToRow (ToRow (toRow))
import Database.PostgreSQL.Simple.ToField (ToField (toField))
import Data.ByteString.Char8 (ByteString)

data User = User { userId :: Int, name :: ByteString } deriving (Show, Eq, Ord)

instance ToRow User where
  toRow User{..} = [ toField userId, toField name ]

instance ToField User where
  toField User{..} = toField userId

data Forum = Forum { forumId :: Int, forumName :: String } deriving (Show)

instance ToRow Forum where
  toRow Forum{..} = [ toField forumId, toField forumName ]

instance ToField Forum where
  toField Forum{..} = toField forumId

data Thread = Thread { threadId :: Int, forum :: Int, threadName :: ByteString, threadPoster :: Maybe User} deriving (Show)

instance ToRow Thread where
  toRow Thread{..} = [ toField threadId, toField forum, toField threadName, toField threadPoster]

instance ToField Thread where
  toField Thread{..} = toField threadId

data Post = Post
  { postId :: Int
  , user :: Maybe User
  , time :: Int
  , thread :: Thread
  , guestName :: Maybe ByteString
  } deriving (Show)

instance ToRow Post where
  toRow Post{..} = [ toField postId, toField thread, toField time, toField user, toField guestName ]

instance ToField Post where
  toField Post{..} = toField postId
