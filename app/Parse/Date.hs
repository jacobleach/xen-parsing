module Parse.Date
  (
    getUnixTimeFromPostHtml
  ) where

import Data.ByteString.Char8 (ByteString, unpack)
import Data.Time.Format (formatTime, parseTimeOrError, defaultTimeLocale)
import Data.Time.LocalTime (LocalTime, TimeZone, localTimeToUTC)
import Text.HTML.TagSoup (innerText, Tag (TagOpen), (~==))
import qualified Data.ByteString.Char8 as BS (filter)

import Debug.Trace (trace)

import Parse.Post.Internal

getUnixTimeFromPostHtml :: TimeZone -> PostHtml -> Maybe Int
getUnixTimeFromPostHtml timeZone postHtml = case getDateFromPostHtml postHtml of
  Nothing -> Nothing
  Just xs -> Just $ getUnixTimeFromDate timeZone xs

getUnixTimeFromDate :: TimeZone -> LocalTime -> Int
getUnixTimeFromDate timeZone = (\x -> read x :: Int) . (formatTime defaultTimeLocale "%s") . (localTimeToUTC timeZone)

getDateFromPostHtml :: PostHtml -> Maybe LocalTime
getDateFromPostHtml (PostHtml header _) = case getDateStringFromPostHeader header of
  Nothing -> Nothing
  Just xs -> Just $ (getDateFromString . unpack) xs

getDateStringFromPostHeader :: PostHeaderHtml -> Maybe ByteString
getDateStringFromPostHeader (PostHeaderHtml headerHtml) = case dropWhile (not . tagHasDate) headerHtml of
  [] -> trace ("Failed to find tag with date: " ++ (show headerHtml)) Nothing
  xs -> case checkLength xs of
    Nothing -> trace "Length of remaining tags not enough to get date" Nothing
    Just as -> case (innerText . take 2) as of
      "" -> trace ("Inner text got empty string on: " ++ (show as)) Nothing
      bs -> case BS.filter (/= '\n') bs of
        "" -> trace ("Filtered down to nothing in String: " ++ (show bs) ++ ", in tag: " ++ (show $ take 3 as)) Nothing
        cs -> Just cs
  where
  checkLength ys
    | (length ys) < 2 = trace "Failed to get enough tabs after finding date tag" Nothing
    | otherwise = Just ys

tagHasDate :: Tag ByteString -> Bool
tagHasDate tag = tag ~== ("<a class=datePermalink>" :: String)

getDateFromString dateByteString = parseTimeOrError True defaultTimeLocale "%b %-d, %Y at %-I:%M %p" dateByteString :: LocalTime
