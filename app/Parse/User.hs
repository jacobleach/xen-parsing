module Parse.User
  (
    getUserFromPostHtml
  , getAuthorName
  , getThreadUserFromHtml
  ) where

import Data.Char (isDigit)
import Data.ByteString.Char8 (ByteString, unpack)
import Text.HTML.TagSoup (maybeTagText, fromAttrib, Tag (TagOpen), (~==))
import Text.Regex.Posix ((=~))
import Text.Read (readMaybe)

import Debug.Trace (trace)

import Parse.Post.Internal (PostHtml (PostHtml), PostBodyHtml(PostBodyHtml), PostHeaderHtml(PostHeaderHtml))
import Types (User(..))

getThreadUserFromHtml :: [Tag ByteString] -> Maybe User
getThreadUserFromHtml html = case dropWhile (not . (~== tag)) html of
  [] -> trace "Did not find the title bar tag" Nothing
  xs -> do
    let tags = (take 2 . drop 12) xs
    name <- (maybeTagText . last) tags
    id <- (getAuthorIdFromNode . head) tags
    return (User id name)

  where tag = TagOpen "div" [("class", "titleBar")] :: Tag ByteString

getUserFromPostHtml :: PostHtml -> Either (Maybe ByteString) (Maybe User)
getUserFromPostHtml (PostHtml _ body) = case getAuthorIdFromPost body of
  Nothing -> Left (getAuthorName body)
  Just id -> case getAuthorName body of
    Nothing   -> Right Nothing
    Just name -> (Right . Just) (User id name)

getAuthorName :: PostBodyHtml -> Maybe ByteString
getAuthorName postBody = do
  authorNameTag <- getAuthorNameTag postBody
  return $ fromAttrib ("data-author" :: ByteString) authorNameTag

getAuthorNameTag :: PostBodyHtml -> Maybe (Tag ByteString)
getAuthorNameTag (PostBodyHtml postTags) = case filter hasAuthorName postTags of
  [] -> trace "Did not find author name tag" Nothing
  x:xs -> Just x

hasAuthorName :: Tag ByteString -> Bool
hasAuthorName tag = tag ~== TagOpen ("li" :: ByteString) [("data-author", "")]

getAuthorIdFromPost:: PostBodyHtml -> Maybe Int
getAuthorIdFromPost postBody = do
  authorUrl <- getAuthorIdUrlFromPost postBody
  getAuthorIdFromUrl authorUrl

getAuthorIdFromUrl :: ByteString -> Maybe Int
getAuthorIdFromUrl author = case author =~ ("([0-9])*\\/?$" :: ByteString) :: ByteString of
  "" -> Nothing
  xs -> (readMaybe . (takeWhile isDigit) . unpack) xs

getAuthorIdFromNode :: Tag ByteString -> Maybe Int
getAuthorIdFromNode tag = do
  url <- getAuthorIdUrlFromNode tag
  id <- getAuthorIdFromUrl url
  return id

getAuthorIdUrlFromNode :: Tag ByteString -> Maybe ByteString
getAuthorIdUrlFromNode tag@(TagOpen _ _) = case fromAttrib "href" tag of
  "" -> Nothing
  xs -> Just xs
getAuthorIdUrlFromNode tag = trace ("tag is not a tagopen: " ++ (show tag)) Nothing

getAuthorIdUrlFromPost:: PostBodyHtml -> Maybe ByteString
getAuthorIdUrlFromPost (PostBodyHtml bodyHtml) = case dropWhile (not . isMemberLink) bodyHtml of
  [] -> Nothing
  xs -> (getAuthorIdUrlFromNode . head) xs

isMemberLink :: Tag ByteString -> Bool
isMemberLink tag = isLinkTag tag && fromAttrib "href" tag =~ ("search/member\\?user_id=[0-9]*" :: ByteString)

isLinkTag :: Tag ByteString -> Bool
isLinkTag tag = tag ~== TagOpen ("a" :: ByteString) [("href", "")]
