module Parse.Post
  (
    getPostsFromHtml
  ) where

import Data.ByteString.Char8 (ByteString, unpack)
import Data.List.Split (splitWhen)
import Text.HTML.TagSoup (fromAttrib, Tag (TagOpen, TagClose), (~==))
import Text.Regex.Posix ((=~))
import qualified Data.ByteString.Char8 as BS (length, drop)
import Data.Time.LocalTime (TimeZone)
import Text.Read (readMaybe)

import Types (User, Post(Post), Thread)
import Parse.Post.Internal
import Parse.User
import Parse.Date

import Debug.Trace

import Control.Monad.Except

data PostParseError = UnknownPostParseError String |
                      PostStringIdParseError ByteString |
                      PostTagIdParseError (Tag ByteString) |
                      PostsListStartNotFound |
                      PostsListEndNotFound |
                      PostListHasExtraOl |
                      PostHeaderNotFound |
                      PostBodyNotFound |
                      PostBodyEmpty

instance Show PostParseError where
  show (UnknownPostParseError string) = string
  show (PostStringIdParseError string) = "Unable to parse the post id from: '" ++ (show string) ++ "'"
  show (PostTagIdParseError tag) = "Unable to parse the post id from: '" ++ (show tag) ++ "'"
  show PostsListStartNotFound = "Unable to find the start of the post list"
  show PostsListEndNotFound = "Unable to find the end of the post list"
  show PostListHasExtraOl = "Post list has an extra <ol> that will break parsing"
  show PostHeaderNotFound = "Post header unable to be found"
  show PostBodyNotFound = "Post body unable to be found"
  show PostBodyEmpty = "Post body was found but is empty"

getPostsFromHtml :: (MonadError PostParseError m) => TimeZone -> Thread -> [Tag ByteString] -> m [Post]
getPostsFromHtml timeZone thread html = do
  messages <- getMessages html
  sequence $ map (getPostFromPostHtml timeZone thread) messages

getPostFromPostHtml :: (MonadError PostParseError m) => TimeZone -> Thread -> PostHtml -> m Post
getPostFromPostHtml timeZone thread post@(PostHtml header body) = case old post timeZone thread body of
  Nothing -> throwError $ UnknownPostParseError "old returned nothing"
  Just (p, t, th, gu) -> do
    id <- getPostId body
    return $ Post id p t th gu

old post timeZone thread body = do
  let user = getUser post
  time <- getUnixTimeFromPostHtml timeZone post
  return ((snd user), time, thread, (fst user))

getUser :: PostHtml -> (Maybe ByteString, Maybe User)
getUser post = case getUserFromPostHtml post of
  Left Nothing  -> trace "BAD BAD BAD" (Nothing, Nothing)
  Left name     -> (name, Nothing)
  Right Nothing -> trace "ALSO BAD BAD BAD" (Nothing, Nothing)
  Right user     -> (Nothing, user)

getPostId :: (MonadError PostParseError m) => PostBodyHtml -> m Int
getPostId (PostBodyHtml []) = throwError $ UnknownPostParseError "getPostId was passed an empty array of tags"
getPostId (PostBodyHtml bodyHtml) = do
  let idTag = head bodyHtml
  let idAsString = fromAttrib ("id" :: ByteString) idTag
  case (BS.length idAsString) of
    0 -> throwError $ PostTagIdParseError idTag
    _ -> getPostIdFromString idAsString

getPostIdFromString :: (MonadError PostParseError m) => ByteString -> m Int
getPostIdFromString "" = throwError $ PostStringIdParseError ""
getPostIdFromString xs
  | ((BS.length xs) <= 5) = throwError $ PostStringIdParseError xs
  | otherwise = case (readMaybe . unpack . BS.drop 5) xs of
    Nothing -> throwError $ PostStringIdParseError xs
    (Just a) -> return a

getMessages :: (MonadError PostParseError m) => [Tag ByteString] -> m [PostHtml]
getMessages html = do
  postList <- getMessageList html
  (sequence . map getPostHtml . drop 1 . splitWhen isMessageHeaderStart) postList

getMessageList :: (MonadError PostParseError m) => [Tag ByteString] -> m [Tag ByteString]
getMessageList html = do
  withoutHeader <- getRidOfHeader html
  getRidOfFooter withoutHeader

getRidOfHeader :: (MonadError PostParseError m) => [Tag ByteString] -> m [Tag ByteString]
getRidOfHeader html = do
  case dropWhile (not . isMessageListStart) html of
    [] -> throwError PostsListStartNotFound
    xs -> return xs

getRidOfFooter :: (MonadError PostParseError m) => [Tag ByteString] -> m [Tag ByteString]
getRidOfFooter html = do
  case takeWhile (\x -> not $ (isEndOfMessages x || isOpenOl x)) html of
    [] -> throwError PostsListEndNotFound
    xs -> checkForBadHtml xs
    where
      checkForBadHtml ys
        | isOpenOl (last ys) = throwError PostListHasExtraOl
        | otherwise = return ys
      isOpenOl tag = tag ~== (TagOpen ("<ol>" :: ByteString) [])

getPostHtml :: (MonadError PostParseError m) => [Tag ByteString] -> m  PostHtml
getPostHtml html = do
  messageHeader <- getNextMessageHeaderHtml html
  messaseBody <- getNextMessageBodyHtml html
  return (PostHtml messageHeader messaseBody)

getNextMessageHeaderHtml :: (MonadError PostParseError m) => [Tag ByteString] -> m PostHeaderHtml
getNextMessageHeaderHtml html = case takeWhile (not . isMessageStart) html of
  [] -> throwError PostHeaderNotFound
  xs -> return $ PostHeaderHtml xs

getNextMessageBodyHtml :: (MonadError PostParseError m) => [Tag ByteString] -> m PostBodyHtml
getNextMessageBodyHtml html = case dropWhile (not . isMessageStart) html of
  [] -> throwError PostBodyNotFound
  xs -> case takeWhile (\tag -> not $ (isMessageHeaderStart tag || isEndOfMessages tag)) xs of
    [] -> throwError PostBodyEmpty
    ys -> return $ PostBodyHtml ys

isMessageHeaderStart :: Tag ByteString -> Bool
isMessageHeaderStart = (~== messageHeaderTag)
  where messageHeaderTag = TagOpen "li" [("class", "msgHeader")] :: Tag ByteString

isEndOfMessages :: Tag ByteString -> Bool
isEndOfMessages = (~== (TagClose ("ol" :: ByteString)))

isMessageStart :: Tag ByteString -> Bool
isMessageStart tag = (tag ~== TagOpen ("li" :: ByteString) [("id", "")]) && (isPostId $ fromAttrib "id" tag)

isPostId :: ByteString -> Bool
isPostId = ((flip (=~)) ("post-[-1-9]*" :: ByteString))

isMessageListStart :: Tag ByteString -> Bool
isMessageListStart = (~== messageListTag)
  where messageListTag = TagOpen "ol" [("class", "messageList")] :: Tag ByteString
