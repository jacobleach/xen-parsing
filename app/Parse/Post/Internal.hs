module Parse.Post.Internal
  (
    PostBodyHtml (PostBodyHtml)
  , PostHeaderHtml (PostHeaderHtml)
  , PostHtml ( .. )
  ) where

import Data.ByteString.Char8 (ByteString)
import Text.HTML.TagSoup (Tag)

data PostHeaderHtml = PostHeaderHtml [Tag ByteString] deriving (Show)
data PostBodyHtml = PostBodyHtml [Tag ByteString] deriving (Show)

data PostHtml = PostHtml { header :: PostHeaderHtml, body :: PostBodyHtml } deriving (Show)
