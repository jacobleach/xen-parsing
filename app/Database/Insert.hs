module Database.Insert
  (
    createConnection
  , insertPost
  , insertUser
  , insertThread
  ) where

import Database.PostgreSQL.Simple

import Types

createConnection :: ConnectInfo -> IO (Connection)
createConnection connectInfo = do
  connection <- connect connectInfo
  prepareStatements connection
  return connection

prepareStatements :: Connection -> IO ()
prepareStatements connection = do
  execute_ connection insertPostProcedure
  execute_ connection insertThreadProcedure
  execute_ connection insertUserProcedure
  return ()

insertPostProcedure :: Query
insertPostProcedure =
  "PREPARE insertPost (int, int, int, int, text)\
  \AS INSERT INTO posts (id, thread_id, created_at, user_id, guest_name) VALUES ($1, $2, TO_TIMESTAMP($3), $4, $5)\
  \ON CONFLICT (id) DO UPDATE \
  \SET user_id = EXCLUDED.user_id, guest_name = EXCLUDED.guest_name"

insertThreadProcedure :: Query
insertThreadProcedure =
  "PREPARE insertThread (int, int, text, int)\
  \AS INSERT INTO threads (id, forum_id, name, user_id) VALUES ($1, $2, $3, $4)\
  \ON CONFLICT (id) DO UPDATE SET forum_id = EXCLUDED.forum_id, name = EXCLUDED.name, user_id = EXCLUDED.user_id"

insertUserProcedure :: Query
insertUserProcedure =
  "PREPARE insertUser (int, text)\
  \AS INSERT INTO users (id, name)\
  \VALUES ($1, $2)\
  \ON CONFLICT (id) DO NOTHING"

insertPost :: Post -> Connection -> IO ()
insertPost post connection = do
  execute connection "execute insertPost(?, ?, ?, ?, ?)" post
  return ()

insertUser :: User -> Connection -> IO ()
insertUser user connection = do
  execute connection "execute insertUser(?, ?)" user
  return ()

insertThread :: Thread -> Connection -> IO ()
insertThread thread connection = do
  execute connection "execute insertThread(?, ?, ?, ?)" thread
  return ()

