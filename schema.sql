CREATE TABLE forums (
    id integer NOT NULL,
    name text NOT NULL
);

CREATE TABLE posts (
    id integer NOT NULL,
    thread_id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    guest_name text,
    CONSTRAINT user_id_xor_guest_name_not_null CHECK (((user_id IS NULL) <> (guest_name IS NULL)))
);

CREATE TABLE threads (
    id integer NOT NULL,
    forum_id integer,
    user_id integer,
    name text
);

CREATE TABLE users (
    id integer NOT NULL,
    name text NOT NULL
);

ALTER TABLE ONLY forums
    ADD CONSTRAINT forums_pkey PRIMARY KEY (id);

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);

ALTER TABLE ONLY threads
    ADD CONSTRAINT thread_pkey PRIMARY KEY (id);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_thread_id_fkey FOREIGN KEY (thread_id) REFERENCES threads(id);

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY threads
    ADD CONSTRAINT threads_forum_id_fkey FOREIGN KEY (forum_id) REFERENCES forums(id);

INSERT INTO forums (id, name) VALUES
    (226, 'General Discussion'),
    (237, 'Feedback'),
    (244, 'Something For All'),
    (338, 'Video Clips'),
    (424, 'Suggestions'),
    (454, 'Dispute Forum Archive'),
    (484, 'Music'),
    (519, 'Movies/Television'),
    (537, 'Sports, Health & Fitness'),
    (558, 'Technology');
